Feature: list candidat

  Scenario: Asking for list candidat page
    When I request ('GET"/list-candiat/)
    Then Get array of all candidats object
    And  Instansiate Entretien
    And  Construct  a form from EntrienType passed in argument
    And  Render a view with created formView and array of candidats

  Scenario: Ajax Submiting a form
    When I submit a form with ajax and  idCandidat passed in url
    Then Testing if request handled and form is valid
    And  Getting submited Data
    And  Get a specific candidat object  with idCandiat passed in parameter
    And  Set somme Atrributes
    And  Persiste entretien object and flush to save in database
    And  Retourn a json response