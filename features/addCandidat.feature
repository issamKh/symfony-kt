Feature: Add candidat

  Scenario: Asking for add candidat page
    When I request ('GET"/add-candiat)
    Then Instansiate Candidat
    And  Construct  a form from CandidatType passed in argument
    And  Render a view with created formView

  Scenario: Submiting a form
    When I submit a form
    Then Testing if request handled and form is valid
    And  Getting  type file from forrm
    And  Call to service  app_cv_uploader and passe file like a parametter
    And  Set somme Atrributes
    And  Persiste candidat object and flush to save in database
    And  Create success message to session flashbag
    And  Redirecte to same page

