<?php

namespace KT\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class KTUserBundle extends Bundle
{

    public function getParent(){

            return "FOSUserBundle";
    }
}
