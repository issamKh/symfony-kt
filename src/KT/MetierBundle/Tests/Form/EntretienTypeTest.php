<?php
/**
 * Created by PhpStorm.
 * User: issam
 * Date: 01/10/18
 * Time: 19:01
 */

namespace KT\MetierBundle\Form;
use KT\MetierBundle\Entity\Entretien;
use KT\MetierBundle\Form\EntretienType;
use Symfony\Component\Form\Test\TypeTestCase;


class EntretienTypeTest extends TypeTestCase {

    public function testSubmitValidDataEntretien()
    {
        $formDataEn = array(

            'responsable'=>'issam',
            'note'=>'tes',
        );
        //---- pour recuperer les données soumis au formulaire
        $entretiencompare = new Entretien();

        // verfier si CandidatType est compiler
        $form = $this->factory->create('KT\MetierBundle\Form\EntretienType', $entretiencompare);

        $entretien = new Entretien();
        $entretien->setResponsable($formDataEn['responsable']);
        $entretien->setNote($formDataEn['note']);

        // valider les données au formulaire
        $form->submit($formDataEn);
        //------pour tester si aucune transformation
        $this->assertTrue($form->isSynchronized());

        // verifier si $candidatcompare a eté   modifié comme prévu qd les forms a ete soumis
        $this->assertEquals($entretien, $entretiencompare);


        //------- pour verifier si  la creation du formView
        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formDataEn) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }
} 