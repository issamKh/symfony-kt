<?php
/**
 * Created by PhpStorm.
 * User: issam
 * Date: 01/10/18
 * Time: 13:58
 */

namespace KT\MetierBundle\Form;

use KT\MetierBundle\Entity\Candidat;
use KT\MetierBundle\Form\CandidatType;
use Symfony\Component\Form\Test\TypeTestCase;


class CandidatTypeTest extends  TypeTestCase {




    public function testSubmitValidData()
    {
        $formData = array(
            'prenom' => 'test',
            'nom' => 'test2',
            'email'=>'test3@gmail.com',
        );
        //---- pour recuperer les données soumis au formulaire
        $candidatcompare = new Candidat();

        // verfier si CandidatType est compiler
        $form = $this->factory->create('KT\MetierBundle\Form\CandidatType', $candidatcompare);

        $object = new Candidat();
        $object->setNom($formData['nom']);
        $object->setPrenom($formData['prenom']);
        $object->setEmail($formData['email']);

        // valider les données au formulaire
        $form->submit($formData);
        //------pour tester si aucune transformation
        $this->assertTrue($form->isSynchronized());

        // verifier si $candidatcompare a eté   modifié comme prévu qd les form a été soumis
        $this->assertEquals($object, $candidatcompare);

        //------- pour verifier  la creation du formView
        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }

} 