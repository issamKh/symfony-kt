<?php

namespace KT\MetierBundle\Controller;

use KT\MetierBundle\Entity\Candidat;
use KT\MetierBundle\Entity\Entretien;
use KT\MetierBundle\Entity\Mails;
use KT\MetierBundle\Form\CandidatType;
use KT\MetierBundle\Form\EntretienType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;


class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $rep = $em->getRepository("KTUserBundle:User");
        $user = $rep->findOneById(2);
        $user->removeRole("Role_superviseur");

        $em->flush();
        return $this->render('KTMetierBundle:Default:index.html.twig');

    }

    /**
     * @Route("/add-candidat")
     */
    public function addCandidatAction(Request $request)

    {
        $candidat = new Candidat();
        $form = $this->createForm(new CandidatType(), $candidat);

        if ($form->handleRequest($request)->isValid()) {

            $file = $candidat->getCv();

            //----appel au service de l'upload
            $fileName = $this->get('app.cv_uploader')->upload($file);

            $em = $this->getDoctrine()->getManager();
            $candidat->setFirst(0);
            $candidat->setSeconde(0);
            $candidat->setTests(0);
            $candidat->setAdmis(0);
            $candidat->setCv($fileName);
            $em->persist($candidat);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Candidat enregistré avec succé .');

            //----redirection a la meme page
            return $this->redirect($request->getUri());

        }

        return $this->render('KTMetierBundle:Default:addCandidat.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/list-candidat/")
     */
    public function listCandidatAction(Request $request)

    {
        //------la liste des candidats à retourner à la vue
        $em = $this->getDoctrine()->getManager();
        $rep = $em->getRepository("KTMetierBundle:Candidat");
        $candidat = $rep->findAll();


        $entretien = new Entretien();
        $form = $this->createForm(new EntretienType(), $entretien);

        if ($form->handleRequest($request)->isValid()) {

            //------recuperation des données soumis au formulaire
            $data = $form->getData();

            //-------recuperation du candidat a qui on prepare l'entretien
            $rep = $em->getRepository("KTMetierBundle:Candidat");
            $cand = $rep->find($request->get('idCandidat'));

            //--------en change la valeur du champs selon le type de l entretien
            if($data->getType() == "premier"){
                $cand->setFirst(1);
            }else{
                $cand->setSeconde(1);
            }

            //-------enregistrement de details de l'entretien avec  idCandidat envoyé de la vue
            $entretien->setIdCandidat($request->get('idCandidat'));
            $em->persist($entretien);
            $em->flush();

            //--------retourn une reponse json
            $res = new JsonResponse();
            return $res->setData(array("response" => "OK"));
        }

        return $this->render('KTMetierBundle:Default:listCandidat.html.twig', array(
            'candidat' => $candidat,
            'form' => $form->createView(),
        ));

    }

    //----------action pour l'envoie des tests par mail
    /**
     * @Route("/envoyer-tests/")
     */
    public function envoieTestAction(Request $request)

    {
        if($request->getMethod() == "POST"){
            $em = $this->getDoctrine()->getManager();
            $sujet = $request->get('sujet');
            $text = $request->get('text');
            $idCandidat = $request->get('idCandidat');
            $from = $request->get('email');

            //-------l'email de l'utilisateur courrant
            $to=$this->getUser()->getEmail();

            $message = \Swift_Message::newInstance()
                ->setContentType('text/html')
                ->setSubject($sujet)
                ->setFrom($from)
                ->setTo($to)
                ->setBody($text);

            $this->get('mailer')->send($message);

            //-------ajout detail du mail
            $mailTest = new Mails();
            $mailTest->setIdCandidat($idCandidat);
            $mailTest->setSujet($sujet);
            $mailTest->setText($text);
            $mailTest->setDate(new \DateTime());
            $em->persist($mailTest);

            //------modification du  candidat
            $rep = $em->getRepository("KTMetierBundle:Candidat");
            $candidat = $rep->find($idCandidat);
            $candidat->setTests(1);
            $em->flush();

            //-----retour reponse json
            $res = new JsonResponse();
            return $res->setData(array("response" => "OK"));


        }else{
            //-----retour reponse json
            $res = new JsonResponse();
            return $res->setData(array("response" => "NotOK"));
        }

    }

    //------profil candidat
    /**
     * @Route("/profil/")
     */
    public function profilAction(Request $request)

    {

        $em = $this->getDoctrine()->getManager();
        $idCandidat = $request->get("idCandidat");

        //------ le candidat
        $repository = $em->getRepository("KTMetierBundle:Candidat");
        $candidat = $repository->find($idCandidat);

        //------si candidat inexistant on fait une redirection
        if(!$candidat){
            return $this->redirect("/list-candidat/");
        }

        //------entretiens
        $repository = $em->getRepository("KTMetierBundle:Entretien");
        $firstEntretien = $repository->findOneBy(array("idCandidat"=>$idCandidat,"type"=>"premier"));
        $secondeEntretien = $repository->findOneBy(array("idCandidat"=>$idCandidat,"type"=>"deuxieme"));

        //------tests
        $repository = $em->getRepository("KTMetierBundle:Mails");
        $tests =  $repository->findOneByIdCandidat($idCandidat);


        return $this->render('KTMetierBundle:Default:profil.html.twig',array(
                'candidat' => $candidat,
                'firstEntretien' => $firstEntretien,
                'secondeEntretien' => $secondeEntretien,
                'tests' => $tests,

            )
        );

    }

    //------calendrier des entretiens

    /**
     * @Route("/calendrier")
     */
    public function calendrierAction()
    {

        $calendrier =array();
        $em = $this->getDoctrine()->getManager();

        //------ la liste des entretiens
        $repository = $em->getRepository("KTMetierBundle:Entretien");
        $entretiens = $repository->findAll();

        //------on parcours la liste des entretien pour recuperer les candidats
        for($i=0;$i<count($entretiens);$i++){

            $calendrier[$i]['entretien'] = $entretiens[$i];
            $repository = $em->getRepository("KTMetierBundle:Candidat");
            $candidat = $repository->find($entretiens[$i]->getIdCandidat());
            $calendrier[$i]['candidat'] = $candidat;

        }

        //--------pour serialiser l'objet et l'encoder en json
        $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new
        JsonEncoder()));
        $calendrier = $serializer->serialize($calendrier, 'json');


        return $this->render('KTMetierBundle:Default:calendrier.html.twig', array(
            'calendrier' => $calendrier,
        ));


    }




}
