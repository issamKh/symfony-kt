<?php

namespace KT\MetierBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Candidat
 *
 * @ORM\Table(name="candidat")
 * @ORM\Entity(repositoryClass="KT\MetierBundle\Repository\CandidatRepository")
 */
class Candidat
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="profil", type="string", length=255, nullable=true)
     */
    private $profil;

    /**
     * @var int
     *
     * @ORM\Column(name="first", type="integer", options={"default" : 0})
     */
    private $first;

    /**
     * @var int
     *
     * @ORM\Column(name="tests", type="integer", options={"default" : 0})
     */
    private $tests;

    /**
     * @var int
     *
     * @ORM\Column(name="seconde", type="integer", options={"default" : 0})
     */
    private $seconde;

    /**
     * @var int
     *
     * @ORM\Column(name="admis", type="integer", options={"default" : 0})
     */
    private $admis;



    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Veuillez choisir un fichier pdf")
     * @Assert\File(mimeTypes={ "application/pdf" })
     */
    private $cv;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Candidat
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Candidat
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Candidat
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set profil
     *
     * @param string $profil
     *
     * @return Candidat
     */
    public function setProfil($profil)
    {
        $this->profil = $profil;

        return $this;
    }

    /**
     * Get profil
     *
     * @return string
     */
    public function getProfil()
    {
        return $this->profil;
    }


    public function setCv($cv)
    {
        $this->cv = $cv;

        return $this;
    }


    public function getCv()
    {
        return $this->cv;
    }

    /**
     * Get first
     *
     * @return integer
     */
    public function getFirst()
    {
        return $this->first;
    }

    /**
     * Set first
     *
     * @param integer $first
     *
     * @return Candidat
     */
    public function setFirst($first)
    {
        $this->first = $first;

        return $this;
    }


    /**
     * Get tests
     *
     * @return integer
     */
    public function getTests()
    {
        return $this->tests;
    }

    /**
     * Set tests
     *
     * @param integer $tests
     *
     * @return Candidat
     */
    public function setTests($tests)
    {
        $this->tests = $tests;

        return $this;
    }

    /**
     * Get seconde
     *
     * @return integer
     */
    public function getSeconde()
    {
        return $this->seconde;
    }

    /**
     * Set seconde
     *
     * @param integer $seconde
     *
     * @return Candidat
     */
    public function setSeconde($seconde)
    {
        $this->seconde = $seconde;

        return $this;
    }

    /**
     * Get admis
     *
     * @return integer
     */
    public function getAdmis()
    {
        return $this->admis;
    }

    /**
     * Set admis
     *
     * @param integer $admis
     *
     * @return Candidat
     */
    public function setAdmis($admis)
    {
        $this->admis = $admis;

        return $this;
    }



}

