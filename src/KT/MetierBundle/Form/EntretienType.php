<?php

namespace KT\MetierBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EntretienType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //$builder->add('idCandidat')->add('type')->add('date')->add('responsable')->add('note');

        $builder

            ->add('type',           'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                'choices'  => array(
                    'Premiere rencontre' => 'premier',
                    'Deuxiéme rencontre DRH' => "Deuxieme",
                ),
                'choices_as_values' => true))


            ->add('responsable',    'Symfony\Component\Form\Extension\Core\Type\TextType')
            ->add('date',           'Symfony\Component\Form\Extension\Core\Type\DateTimeType',
                    array(
                    'widget' => "single_text",
                    'html5' => false,
                    'date_format'=>"dd/MM/yyyy hh:mm",
                    'attr'=>array(
                        'class'=>'datetimePick form-control',
                        'data-format'=>"dd/MM/yyyy hh:mm"
                        )
                    )
                )

            ->add('note',      'Symfony\Component\Form\Extension\Core\Type\TextareaType')

    ;

    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'KT\MetierBundle\Entity\Entretien'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'kt_metierbundle_entretien';
    }


}
