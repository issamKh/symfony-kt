<?php

namespace KT\MetierBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CandidatType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //$builder->add('nom')->add('prenom')->add('profil')->add('cv');

        $builder
            ->add('nom',       'Symfony\Component\Form\Extension\Core\Type\TextType')
            ->add('prenom',    'Symfony\Component\Form\Extension\Core\Type\TextType')
            ->add('email',     'Symfony\Component\Form\Extension\Core\Type\EmailType')
            ->add('profil',    'Symfony\Component\Form\Extension\Core\Type\TextType')
            ->add('cv',        'Symfony\Component\Form\Extension\Core\Type\FileType')
            ->add('save',      'Symfony\Component\Form\Extension\Core\Type\SubmitType')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'KT\MetierBundle\Entity\Candidat'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'kt_metierbundle_candidat';
    }


}
