<?php
/**
 * Created by PhpStorm.
 * User: issam
 * Date: 01/10/18
 * Time: 09:39
 */

namespace KT\MetierBundle;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CVUploader {

    private $targetDirectory;

    public function __construct($targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
    }

    public function upload(UploadedFile $file)
    {
        //------encoder le nom
        $fileName = md5(uniqid()).'.'.$file->guessExtension();
        //---------deplacer le fichier dans le dossier
        try {
            $file->move($this->getTargetDirectory(), $fileName);
        } catch (FileException $e) {


        }

        return $fileName;
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }
} 